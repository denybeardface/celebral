import { Sprite, Container, Texture } from "pixi.js";
import { TweenMax } from "gsap";
import Hammer from "hammerjs";
import html2canvas from "html2canvas";

const { innerWidth, innerHeight } = window;

const deg2rad = degrees => {
  return (degrees * Math.PI) / 180;
};

class Game {
  constructor(app, cam) {
    this.app = app;
    this.cam = cam;
    this.mode = "picker";
    this.curTat = null;

    this.app.loader
      .add("arrowL", "images/arrow-left@2x.png")
      .add("arrowR", "images/arrow-right@2x.png")
      .add("bot", "images/bot@2x.jpg")
      .add("btnChoose", "images/btn-choose@2x.png")
      .add("btnPhoto", "images/btn-photo@2x.png")
      .add("btnReturn", "images/btn-return@2x.png")
      .add("btnSave", "images/btn-save@2x.png")
      .add("btnTurn", "images/btn-turn@2x.png")
      .add("btnCeleb", "images/btn-site@2x.png")
      .add("logoBot", "images/logo-lays@2x.png")
      .add("logoTop", "images/logo-top@2x.png")
      .add("top", "images/top@2x.jpg")
      .add("selectIm1", "images/select-im-1@2x.jpg")
      .add("selectIm2", "images/select-im-2@2x.jpg")
      .add("selectIm3", "images/select-im-3@2x.jpg")
      .add("selectIm4", "images/select-im-4@2x.jpg")
      .add("selectIm5", "images/select-im-5@2x.jpg")
      .add("selectIm6", "images/select-im-8@2x.jpg")
      .add("disableIm1", "images/disable-im-1@2x.jpg")
      .add("disableIm2", "images/disable-im-2@2x.jpg")
      .add("disableIm3", "images/disable-im-2_2@2x.jpg")
      .add("disableIm4", "images/disable-im-4@2x.jpg")
      .add("disableIm5", "images/disable-im-5@2x.jpg")
      .add("disableIm6", "images/disable-im-6@2x.jpg")
      .add("showcaseIm1", "images/tatoo-1@2x.png")
      .add("showcaseIm2", "images/tatoo-2@2x.png")
      .add("showcaseIm3", "images/tatoo-3@2x.png")
      .add("showcaseIm4", "images/tatoo-4@2x.png")
      .add("showcaseIm5", "images/tatoo-5@2x.png")
      .add("showcaseIm6", "images/tatoo-6@2x.png")
      .add("resizebleIm1", "images/Tatoo_1.png")
      .add("resizebleIm2", "images/Tatoo_2.png")
      .add("resizebleIm3", "images/Tatoo_3.png")
      .add("resizebleIm4", "images/Tatoo_4.png")
      .add("resizebleIm5", "images/Tatoo_5.png")
      .add("resizebleIm6", "images/Tatoo_6.png")
      .load(this.onLoadComplete);
  }

  onLoadComplete = () => {
    this.scenes = {
      picker: this.buildPickerUI,
      camera: this.buildCameraUI,
      postp: this.buildPostPUI
    };

    this.setScene();
  };

  buildPickerUI = () => {
    const { resources } = this.app.loader;
    const container = new Container();
    const top = new Sprite(resources.top.texture);
    const bot = new Sprite(resources.bot.texture);
    const selectBtn = new Sprite(resources.btnChoose.texture);
    const logoBot = new Sprite(resources.logoBot.texture);
    const arrL = new Sprite(resources.arrowL.texture);
    const arrR = new Sprite(resources.arrowR.texture);
    const image1 = new Sprite(resources.disableIm1.texture);
    const image2 = new Sprite(resources.disableIm2.texture);
    const image3 = new Sprite(resources.disableIm3.texture);
    const image4 = new Sprite(resources.disableIm4.texture);
    const image5 = new Sprite(resources.disableIm5.texture);
    const image6 = new Sprite(resources.disableIm6.texture);
    const drPane = new Container();
    const paneBG = new Sprite(Texture.WHITE);
    const STICKER_WIDTH = image1.width * 0.5 + 20;
    const showcase = new Sprite(resources.showcaseIm1.texture);

    drPane.addChild(image1);
    drPane.addChild(image2);
    drPane.addChild(image3);
    drPane.addChild(image4);
    drPane.addChild(image5);
    drPane.addChild(image6);

    container.addChild(top);
    container.addChild(bot);
    container.addChild(showcase);
    container.addChild(selectBtn);
    container.addChild(logoBot);
    container.addChild(paneBG);
    container.addChild(drPane);
    container.addChild(arrL);
    container.addChild(arrR);

    drPane.children.forEach((child, idx) => {
      child.position.x = child.width * idx + 10 * idx;

      child.interactive = true;
      child.buttonMode = true;
      child.on("pointerup", () => {
        if (this.mode !== "picker") return;
        if (this.cam.video.paused) {
          this.cam.video.play();
        }
        if (this.curTat !== null && this.curTat !== idx) {
          drPane.children[this.curTat].texture =
            resources[`disableIm${this.curTat + 1}`].texture;
        }
        this.curTat = idx;
        TweenMax.to(showcase.scale, 0.25, {
          x: 0,
          y: 0,
          onComplete: () => {
            showcase.visible = true;
            child.texture = resources[`selectIm${this.curTat + 1}`].texture;
            showcase.texture =
              resources[`showcaseIm${this.curTat + 1}`].texture;
            TweenMax.to(showcase.scale, 0.25, { x: 0.3, y: 0.3 });
          }
        });
      });
    });

    container.children.forEach(item => {
      item.scale.set(0.5, 0.5);
    });

    top.position.set(0, 0);
    bot.position.set(0, innerHeight - bot.height);

    bot.width = innerWidth;
    top.width = innerWidth;

    showcase.position.set(
      (innerWidth - showcase.width) / 2 + showcase.width / 2,
      (innerHeight + arrL.height + bot.height - showcase.height) / 2
    );
    showcase.anchor.set(0.5, 0.5);
    showcase.scale.set(0.3);
    selectBtn.position.set(
      (bot.width - selectBtn.width) / 2,
      bot.y + (bot.height - selectBtn.height) / 2
    );

    logoBot.position.set(
      innerWidth - logoBot.width - 16,
      innerHeight - logoBot.height - 16
    );

    arrL.position.set(0, bot.y - arrL.height);
    arrR.position.set(innerWidth - arrR.width, bot.y - arrR.height);

    arrL.interactive = true;
    arrL.buttonMode = true;

    arrR.interactive = true;
    arrR.buttonMode = true;

    selectBtn.interactive = true;
    selectBtn.buttonMode = true;

    drPane.position.set(arrL.x + arrL.width, arrL.y);
    paneBG.position.set(arrL.x + arrL.width, arrL.y);
    paneBG.width = innerWidth - arrL.width * 2;
    paneBG.height = arrL.height;

    showcase.visible = true;
    this.curTat = 0;
    drPane.children[0].texture =
      resources[`selectIm${this.curTat + 1}`].texture;

    arrL.on("pointerup", () => {
      if (this.mode !== "picker") return;
      arrL.buttonMode = false;
      this.move(1, drPane, STICKER_WIDTH, arrL);
    });
    arrR.on("pointerup", () => {
      if (this.mode !== "picker") return;

      arrR.buttonMode = false;
      this.move(-1, drPane, STICKER_WIDTH, arrR);
    });

    selectBtn.on("pointerup", () => {
      if (this.mode !== "picker") return;
      if (this.curTat === null) return;
      window.yaCounter52692736.reachGoal("select_tattoo_" + (this.curTat + 1));

      TweenMax.to(container, 0.4, {
        alpha: 0,
        onComplete: () => {
          this.mode = "camera";
          this.setScene();
        }
      });
    });

    return container;
  };

  buildCameraUI = () => {
    const { resources } = this.app.loader;
    const container = new Container();

    const btnPhoto = new Sprite(resources.btnPhoto.texture);
    const btnReturn = new Sprite(resources.btnReturn.texture);
    const btnTurn = new Sprite(resources.btnTurn.texture);
    const logo = new Sprite(resources.logoTop.texture);
    const btnSave = new Sprite(resources.btnSave.texture);
    const btnCeleb = new Sprite(resources.btnCeleb.texture);
    const tattoo = new Sprite(
      resources[`resizebleIm${this.curTat + 1}`].texture
    );
    tattoo.anchor.set(0.5);
    const controlBtns = new Container();
    const postButtons = new Container();

    container.addChild(tattoo);

    controlBtns.addChild(btnReturn);
    controlBtns.addChild(btnPhoto);
    controlBtns.addChild(btnTurn);

    postButtons.addChild(btnSave);
    postButtons.addChild(btnCeleb);

    container.addChild(logo);
    container.addChild(controlBtns);
    container.addChild(postButtons);

    container.children.forEach(item => {
      item.scale.set(0.5, 0.5);
    });

    tattoo.scale.set(0.3, 0.3);

    logo.position.set((innerWidth - logo.width) / 2, 31);
    postButtons.visible = false;

    controlBtns.position.set(
      (innerWidth - (btnReturn.width + btnPhoto.width + btnTurn.width) / 2) /
        2 +
        24 -
        controlBtns.width / 3,
      innerHeight - controlBtns.height - 31
    );

    postButtons.position.set(
      (innerWidth - postButtons.width) / 2,
      innerHeight - postButtons.height - 31
    );

    btnSave.position.y = btnCeleb.position.y - btnSave.height - 30;

    btnReturn.position.set(0, 0);
    btnPhoto.position.set(btnReturn.x + btnReturn.width + 12, -31);
    btnTurn.position.set(btnPhoto.x + btnPhoto.width + 12, 0);
    tattoo.position.set(
      (innerWidth - tattoo.width) / 2 + tattoo.width / 2,
      (innerHeight - tattoo.height) / 2 + tattoo.height / 2
    );

    btnReturn.interactive = true;
    btnReturn.buttonMode = true;

    btnReturn.on("pointerup", () => {
      if (this.mode !== "camera") return;
      this.tattoItem = undefined;
      this.curTat = null;
      TweenMax.to(container, 0.4, {
        alpha: 0,
        onComplete: () => {
          this.mode = "picker";
          this.setScene();
        }
      });
    });
    this.tattoItem = tattoo;

    const hammer = new Hammer.Manager(document.body);

    const pinch = new Hammer.Pinch();
    const rotate = new Hammer.Rotate();
    const pan = new Hammer.Pan();

    hammer.add([pan, pinch, rotate]);

    pinch.recognizeWith(rotate);

    hammer.on("pan", this.dragMove);
    hammer.on("pinch rotate", this.dragTransform);

    btnPhoto.interactive = true;
    btnPhoto.buttonMode = true;

    btnPhoto.on("pointerup", () => {
      this.tattoItem.interactive = false;
      window.yaCounter52692736.reachGoal("snapshot_taken");
      TweenMax.to(controlBtns, 0.3, {
        alpha: 0,
        onComplete: () => {
          this.cam.stop();
          postButtons.visible = true;
        }
      });
    });

    btnTurn.interactive = true;
    btnTurn.buttonMode = true;

    btnTurn.on("pointerup", () => {
      this.cam.toggleMode();
    });

    btnSave.interactive = true;
    btnSave.buttonMode = true;

    btnSave.on("pointerup", () => {
      hammer.off("pan", this.dragMove);
      hammer.off("pinch rotate", this.dragTransform);
      window.yaCounter52692736.reachGoal("snapshot_saved");

      TweenMax.staggerTo(
        [logo, postButtons],
        0.3,
        {
          alpha: 0
        },
        0,
        () => {
          html2canvas(document.body).then(canvas => {
            document.body.appendChild(canvas);
            const a = document.createElement("a");
            a.setAttribute("download", "image" + Date.now());
            a.href = canvas.toDataURL();
            a.click();
            a.remove();
            TweenMax.staggerTo([logo, postButtons], 0.3, {
              alpha: 1
            });
          });
        }
      );
    });

    btnCeleb.interactive = true;
    btnCeleb.buttonMode = true;

    btnCeleb.on("pointerup", () => {
      window.yaCounter52692736.reachGoal("link_activated");
      window.location.href = "http://laysmusic.ru";
    });
    requestAnimationFrame(this.render);

    return container;
  };

  render = () => {
    requestAnimationFrame(this.render);
    this.app.renderer.render(this.app.stage);
  };

  dragMove = e => {
    if (this.mode !== "camera" || e.isFinal) return;
    var newPos = e.center;
    if (newPos.x <= this.tattoItem.width * this.tattoItem.scale.x) {
      newPos.x = this.tattoItem.width * this.tattoItem.scale.x;
    } else if (
      newPos.x >
      innerWidth - this.tattoItem.width * this.tattoItem.scale.x
    ) {
      newPos.x = innerWidth - this.tattoItem.width * this.tattoItem.scale.x;
    }
    if (newPos.y <= this.tattoItem.height * this.tattoItem.scale.y) {
      newPos.y = this.tattoItem.height * this.tattoItem.scale.y;
    } else if (
      newPos.y >
      innerHeight - this.tattoItem.height * this.tattoItem.scale.y
    ) {
      newPos.y = innerHeight - this.tattoItem.height * this.tattoItem.scale.y;
    }

    this.tattoItem.position.set(newPos.x, newPos.y);
  };

  dragTransform = e => {
    const { scale, rotation } = e;

    if (scale / 5 > 0.4) {
      this.tattoItem.scale.set(0.4);
      return;
    }
    this.tattoItem.scale.set(e.scale / 5);

    this.tattoItem.rotation = deg2rad(rotation);
  };

  move = (dir, item, offset, controller) => {
    const distance = item.x + dir * offset;
    if (dir === 1 && distance > controller.width * 3) {
      return;
    }
    if (dir === -1 && distance < -offset * 3) {
      return;
    }

    TweenMax.to(item.position, 0.4, {
      x: distance,
      onComplete: () => (controller.buttonMode = true)
    });
  };

  setScene = () => {
    if (this.scene) {
      this.app.stage.children.map(child => {
        if (child === this.scene) {
          this.app.stage.removeChild(child);
        }
      });
    }
    this.scene = this.scenes[this.mode];
    this.scene.alpha = 1;
    this.app.stage.addChild(this.scenes[this.mode]());
  };
}

export default Game;
