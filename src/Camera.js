import { isRegExp } from "util";

const FACING_MODE = {
  USER: "user",
  ENVIRONMENT: "environment"
};

class Camera {
  constructor() {
    this.mode = FACING_MODE.ENVIRONMENT;
    this.constraints = {
      video: {
        facingMode: this.mode,
        width: 1280
      },
      audio: false
    };
    this.canvas = document.querySelector("#camera");
    this.ctx = this.canvas.getContext("2d");

    this.video = document.createElement("video");
    this.video.setAttribute("muted", true);
    this.video.setAttribute("autoplay", true);
    this.video.setAttribute("playsinline", true);
    this.video.addEventListener("loadedmetadata", () => {
      this.canvas.width = this.video.videoWidth;
      this.canvas.height = this.video.videoHeight;
      this.render();
    });

    window.addEventListener("blur", () => {
      this.video.srcObject = null;
    });

    window.addEventListener("focus", () => {});

    this.startCanvas();
  }

  startCanvas = () => {
    if (navigator.mediaDevices) {
      navigator.mediaDevices
        .getUserMedia(this.constraints)
        .then(this.handleMedia)
        .catch(err => console.log(err.message));
    } else if (navigator.getUserMedia) {
      navigator.getUserMedia(this.constraints, this.handleMedia, err =>
        console.log(err)
      );
    } else {
      console.log("guess I suck");
    }
  };

  handleMedia = stream => {
    this.stream = stream;
    this.video.srcObject = stream;
  };

  stop = () => {
    this.video.pause();
  };

  toggleMode = () => {
    this.mode =
      this.mode === FACING_MODE.USER
        ? FACING_MODE.ENVIRONMENT
        : FACING_MODE.USER;
    this.constraints.video.facingMode = this.mode;
    this.stream.getTracks().map(track => track.stop());
    this.stream = null;
    this.video.srcObject = null;
    this.startCanvas();
  };

  snapshot = () => {
    return this.canvas;
  };

  render = () => {
    window.requestAnimationFrame(this.render);
    this.ctx.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height);
  };
}

export default Camera;
