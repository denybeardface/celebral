import * as PIXI from "pixi.js";
import Game from "./Game";
import Camera from "./Camera";
import "./index.css";

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;

const root = document.querySelector("#root");
const { innerWidth, innerHeight } = window;
const width = innerWidth > 414 ? 414 : innerWidth;

const app = new PIXI.Application({
  width: width,
  height: innerHeight,
  resolution: devicePixelRatio,
  transparent: true,
  autoResize: true
});

const cam = new Camera();
const UI = new Game(app, cam);
root.appendChild(app.view);
